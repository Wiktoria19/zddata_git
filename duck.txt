     Kaczkę natrzeć solą, pieprzem, oraz suszonymi ziołami, wysmarować roztopionym masłem. Do środka można włożyć 1 jabłko pokrojone na ćwiartki z wykrojonymi gniazdami nasiennymi. Kaczkę można od razu zacząć piec lub wstawić do lodówki na 2 - 3 godziny, maksymalnie na całą dobę. Przed pieczeniem kaczkę ocieplić do temperatury pokojowej.
    Piekarnik nagrzać do 230 stopni. Kaczkę włożyć do żaroodpornego garnka (grzbietem do góry) lub naczynia żaroodpornego. Wstawić do nagrzanego piekarnika (na razie bez pokrywki) i piec przez 1/2 godziny. Następnie zamknąć garnek pokrywą, zmniejszyć temperaturę do 180 stopni i piec przez 2 godziny. W połowie pieczenia przewrócić kaczkę na drugą stronę. Na pół godziny przed końcem pieczenia do piekarnika włożyć wydrążone jabłka (do środka jabłek włożyć po łyżeczce konfitury malinowej lub/i wsypać szczyptę cynamonu).
    Wyjąć kaczkę z garnka i podzielić, można polać wytopionym tłuszczem wedle uznania. Tłuszcz z kaczki można zachować i użyć np. do podsmażania ziemniaków lub sporządzenia confit lub smarowidła (rillettes) z pieczonego mięsa.
    Podawać z żurawiną wymieszaną z sokiem lub konfiturą malinową i kluseczkami.


